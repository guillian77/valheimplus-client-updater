Write-Host "--------------------------"
Write-Host "-- VALHEIM PLUS UPDATER --"
Write-Host "--------------------------"
Write-Host "Author: AUFRERE Guillian"
Write-Host "Repository: https://bitbucket.org/guillian77/valheimplus-client-updater"
Write-Host "Description: You can use this executable for personnal use."
Write-Host "             This is an experimental program. Use it"
Write-Host "             Use it at your own risk"

# --
# - Functions
# -
$lastStep = 0
function showStep() {
    Param ([string] $text)

    $Global:lastStep++
    Write-Host [STEP $Global:lastStep] $text
}

# --
# - Define some variable
# -
$repo = "valheimPlus/ValheimPlus"
$file = "WindowsClient.zip"

$releases = "https://api.github.com/repos/$repo/releases"

showStep -text "Determining latest release"
$tag = (Invoke-WebRequest $releases | ConvertFrom-Json)[0].tag_name

$download = "https://github.com/$repo/releases/download/$tag/$file"
$name = $file.Split(".")[0]
$zip = "$name-$tag.zip"
$dir = "$name-$tag"

# --
# - Check current mod version
# -
[bool] $needModUpdate = $false

showStep -text "Checking your current mod version"
if (Test-Path -Path "mod_version.txt") {
    $currModVersion = Get-Content -Path "mod_version.txt"

    if ($currModVersion -ne "$tag") {
        $needModUpdate = $true
    }
    Remove-Item -Path "mod_version.txt" -ErrorAction SilentlyContinue -Force
} else {
    $needModUpdate = $true
}
Write-Host "> Save downloaded version for futur verifications"
New-Item -Path . -Name "mod_version.txt" -ItemType "file" -Value $tag | Out-Null

# --
# - Check mods files presence
# -
showStep -text "Checking mod file presence"
if (![System.IO.Directory]::Exists("BepInEx")) {
    $needModUpdate = $true
} elseif (![System.IO.Directory]::Exists("doorstop_libs")) {
    $needModUpdate = $true
} elseif (![System.IO.File]::Exists("doorstop_config.ini")) {
    $needModUpdate = $true
} else {
    Write-Host "All mod file here. Skip."
}

# --
# - Ugrade mod version with last release
# -
if ($needModUpdate) {
    Write-Host "> Current version is different than last."
    Write-Host "> Need to be updated."

    # --
    # - Download last mod archive
    # -
    showStep -text "Dowloading latest release"
    $webClient = New-Object System.Net.WebClient
    $webClient.UseDefaultCredentials = $true
    $webClient.DownloadFile($download, $zip)

    # --
    # - Unzip the archive
    # -
    showStep -text "Extracting release files"
    Expand-Archive $zip -Force

    # --
    # - Clean target directory
    # -
    Remove-Item $name -Recurse -Force -ErrorAction SilentlyContinue 

    # --
    # - Replace old files with new
    # -
    showStep -text "Get list of modded files and clean"
    $modFiles = Get-ChildItem -Path $dir
    foreach($child in $modFiles) {
        if (Test-Path $child) {
            if ((Get-Item $dir/$child) -is [System.IO.DirectoryInfo]) { # Is folder
                Write-Host "> Clean existing folder $child"
                Remove-Item $child -Force -Recurse
            } else { # Is file
                Write-Host "> Clean existing file $child"
                Remove-Item $child -Force
            }
        }
        
        Move-Item $dir/$child -Destination ./ -Force
    }

    # Removing temp files
    showStep -text "Remove temp files"
    Remove-Item $zip -Force
    Remove-Item $dir -Recurse -Force
} else {
    Write-Host "> Current version is already the last"
    Write-Host "> Don't need to be updated."
}

# --
# - Check steam is running
# -
showStep -text "Check steam is running"
$steamIsRunning = Get-Process steam -ErrorAction SilentlyContinue
if (!$steamIsRunning) {
    &"C:\Program Files (x86)\Steam\steam.exe"
    
    Do{
        Start-Sleep 1
        $steamIsRunning = Get-Process steam -ErrorAction SilentlyContinue
        Write-Host "Wait for steam running"
    }While(!$steamIsRunning)

    Start-Sleep 3
}

# --
# - Start Valheim Game
# -
showStep -text "Start Valheim game"
Start-Process -FilePath "valheim.exe" -WorkingDirectory "./"
