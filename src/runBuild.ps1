$SCRIPTPATH = Split-Path $SCRIPT:MyInvocation.MyCommand.Path -parent
$ToBuildPath = "$SCRIPTPATH\ressources\"
$converter = "$SCRIPTPATH\converter.ps1"

$version = "1.0"
$copyright = "https://guillian-aufrere.fr"

# Convert RSI Launcher for HOTAS PS1 source file.
ls "$ToBuildPath\updater.ps1" | %{
	."$converter" "$($_.Fullname)" "$($_.Fullname -replace 'updater.ps1','ValheimPlus Updater.exe')" `
    -verbose `
    -title "ValheimPlus Updater" `
    -description "ValheimPlus Updater" `
    -version "$version" `
    -product "Easy app to install or update ValheimPlus client mod." `
    -copyright "$copyright" `
    -iconFile "$SCRIPTPATH\ressources\icon.ico"}