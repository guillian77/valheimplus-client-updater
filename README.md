# ValheimPlus Client Updater

This little executable can install or update your ValheimPlus Mod files for your on Windows.

## Features
- Check current mod version
- Auto update mod files if needed
- Check steam is openned before game launching
- Start Valheim game

## Notice

I am not a confirmed Windows application developper.
Make sure to backup your game directory before use my application.

## Installation

Download the [last version](https://bitbucket.org/guillian77/valheimplus-client-updater/downloads/) of ValheimPlus Update.exe and place it inside the root of your game directory.
Double clic on it and wait for updating.